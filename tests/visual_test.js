const { BASE_URL, REGISTER_URL, LOGIN_URL, SEARCH_URL } = require('../data/urls');

Feature('Visual regression');

Scenario('Main page check @visual', async ({ I}) => {
    I.amOnPage(BASE_URL);
    I.saveScreenshot('Main_Page.png');
    I.seeVisualDiff('Main_Page.png', {tolerance: 20, prepareBaseImage: false});
});

Scenario('Registration page check @visual', async ({ I}) => {
    I.amOnPage(REGISTER_URL);
    I.saveScreenshot('Registration_Page.png');
    I.seeVisualDiff('Registration_Page.png', {tolerance: 50, prepareBaseImage: false});
});

Scenario('Login page check @visual', async ({ I}) => {
    I.amOnPage(LOGIN_URL);
    I.saveScreenshot('Login_Page.png');
    I.seeVisualDiff('Login_Page.png', {tolerance: 30, prepareBaseImage: false});
});

Scenario('Search page check @visual', async ({ I}) => {
    I.amOnPage(SEARCH_URL);
    I.saveScreenshot('Search_Page.png');
    I.seeVisualDiff('Search_Page.png', {tolerance: 80, prepareBaseImage: false});
});