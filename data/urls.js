const BASE_URL = 'http://www.qa2.ru/';
const REGISTER_URL = '/create-account';
const LOGIN_URL = '/login'
const SEARCH_SMTH_URL = '/search&search';
const SEARCH_URL = '/search';

module.exports = { BASE_URL, REGISTER_URL, LOGIN_URL, SEARCH_URL, SEARCH_SMTH_URL };
